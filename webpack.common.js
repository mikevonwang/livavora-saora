var path = require('path');
var webpack = require('webpack');

module.exports = {
  entry: './src/global/js/app.js',
  output: {
    path: path.resolve(__dirname, 'dist'),
    publicPath: '/',
    filename: 'bundle.js',
    chunkFilename: 'posts/[name].bundle.js',
  },
  module: {
    rules: [{
      test: /\.js$/,
      exclude: /node_modules/,
      loader: 'babel-loader',
      options: {
        presets: ['@babel/preset-env', '@babel/preset-react'],
        plugins: ['@babel/plugin-transform-runtime']
      },
    }],
  },
  resolve: {
    extensions: ['*', '.js', '.json'],
    alias: {
      'config': path.resolve(__dirname, './config-dist')
    },
  },
  plugins: [
    new webpack.ProvidePlugin({
      React: 'react',
      ReactDOM: 'react-dom',
      Rhaetia: 'rhaetia',
      A: ['rhaetia', 'A'],
      Lieumila: 'lieumila',
      Localizer: ['lieumila', 'Localizer'],
      L: ['lieumila', 'L'],
      Config: 'config',
      R: ['markling', 'render'],
    }),
    new webpack.IgnorePlugin({
      resourceRegExp: /\.localized\.js$/,
    }),
    new webpack.NamedChunksPlugin(function(chunk) {
      if (chunk.name) {
        return chunk.name;
      }
      else {
        for (var module of chunk._modules) {
          return module.rawRequest.split('/').slice(1).join('/').split('.')[0];
        }
        return null;
      }
    }),
  ],
}
