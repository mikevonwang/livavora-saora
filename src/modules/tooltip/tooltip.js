export default function Tooltip(props) {

  const container = React.useRef(null);

  React.useEffect(() => {
    if (!container.current) {
      container.current = document.querySelector('div.tooltip-container');
    }
  });

  if (container.current && props.visible) {
    const parent_rect = props.parent.getBoundingClientRect();
    const style = {
      top: parent_rect.bottom + 'px',
      left: (parent_rect.x + (parent_rect.width / 2)) + 'px',
    };
    return ReactDOM.createPortal((
      <div className='tooltip' style={style}>
        {props.children}
      </div>
    ), container.current);
  }
  else {
    return null;
  }

}
