export default function InputRadiobuttons(props) {

  function setValue(index) {
    if (props.allow_null && props.value === props.options[index]) {
      props.onChange(null);
    }
    else {
      props.onChange(props.options[index]);
    }
  }

  const options = props.options.map((option,i) => {
    const class_name = (option === props.value) ? ' checked' : ' unchecked';
    return (
      <li onClick={(e) => {e.preventDefault(); setValue(i)}} key={'option-' + i}>
        <button className={'radiobutton' + class_name} type='button' disabled={props.disabled}>{option}</button>
        <input type='radio' checked={(option.value === props.value)} readOnly disabled={props.disabled}></input>
      </li>
    );
  });

  return (
    <div className={'input-radiobuttons'}>
      <ul>
        {options}
      </ul>
    </div>
  );

}

InputRadiobuttons.defaultProps = {
  disabled: false,
  allow_null: false,
};
