import { N } from '../../global/js/utilities';

import InputText from '../../modules/inputtext/inputtext';
import InputWord from '../../modules/inputword/inputword';
import InputRadioButtons from '../../modules/inputradiobuttons/inputradiobuttons';

import useFormState from '../../hooks/useFormState';

export default function Form(props) {

  const [is_loading, setIsLoading] = React.useState(true);
  const [reset_trigger, setResetTrigger] = React.useState(true);
  const [form_raw, setForm, is_dirty, setIsDirty, resetForm] = useFormState(props.word);
  const [error, setError] = React.useState(null);

  const form = Object.assign({}, form_raw, {
    parts: form_raw.parts || [],
    definitions: form_raw.definitions || [],
  });

  const ref_form = React.useRef(null);
  const ref_parts = React.useRef(null);
  const ref_definitions = React.useRef(null);

  React.useEffect(() => {
    if (props.word) {
      resetForm(props.word);
      ref_form.current.querySelector('input#word-word').focus();
    }
  }, [props.word, reset_trigger]);

  React.useEffect(() => {
    if (error !== null) {
      setTimeout(() => {
        setError(null);
      }, 2000);
    }
  }, [error]);

  // - - - - -

  function onChangePart(index, value) {
    setForm({
      parts: form.parts.map((part, i) => {
        if (i === index) {
          return value;
        }
        else {
          return part;
        }
      }),
    });
  }

  function onChangeDefinition(definition_i, value) {
    setForm({
      definitions: form.definitions.map((definition, i) => {
        if (i === definition_i) {
          return N(value);
        }
        else {
          return definition;
        }
      }),
    });
  }

  function onAddPart() {
    setForm({
      parts: [
        ...form.parts,
        null,
      ],
    });
    setTimeout(() => {
      ref_parts.current.querySelector('li:last-child input').focus();
    });
  }

  function onRemovePart(index) {
    setForm({
      parts: form.parts.filter((part, i) => {
        return (i !== index);
      }),
    });
  }

  function onAddDefinition() {
    setForm({
      definitions: [
        ...form.definitions,
        '',
      ],
    });
    setTimeout(() => {
      ref_definitions.current.querySelector('li:last-child input').focus();
    });
  }

  function onRemoveDefinition(index) {
    setForm({
      definitions: form.definitions.filter((definition, i) => {
        return (i !== index);
      }),
    });
  }

  async function onSaveWord() {
    const filtered_parts = form.parts.filter(part => part !== null);

    if (form.word === '') {
      setError('word must be defined')
    }
    else if (form.pronunciation === '') {
      setError('pronunciation must be defined');
    }
    else if (/[^\.-]/g.test(form.pronunciation) === true) {
      setError('pronunciation must only contain "." and "-"');
    }
    else if (form.definitions[0] === '') {
      setError('at least one definition must be defined');
    }
    else if (form.type === 'affix' && form.word.indexOf('-') === -1) {
      setError('affixes must include "-"');
    }
    else {
      try {
        await props.onSaveWord({
          id: props.word.id,
          word: form.word,
          type: form.type,
          pronunciation: form.pronunciation,
          parts: (filtered_parts.length > 0) ? filtered_parts : null,
          definitions: form.definitions,
        });
        setIsDirty(false);
      }
      catch (err) {
        switch (err) {
          case 'word_already_exists': {
            setError('word already exists!');
          }
        }
      }
    }
  }

  function onResetWord() {
    setResetTrigger(!reset_trigger);
  }

  // - - - - -

  const parts_JSX = form.parts.map((part, i) => {
    return (
      <li className='row' key={i}>
        <div className='field'>
          <InputWord value={part} onChange={onChangePart.bind(null, i)} currentWord={props.word}/>
        </div>
        <button className='btn' type='button' onClick={onRemovePart.bind(null, i)}>{'×'}</button>
      </li>
    )
  });

  const definitions_JSX = form.definitions.map((definition, i) => {
    return (
      <li className='row' key={i}>
        <p className='list-number'>{(i+1) + '.'}</p>
        <div className='field'>
          <InputText id='word-definition' value={definition} onChange={onChangeDefinition.bind(null, i)}/>
        </div>
        <button className='btn' type='button' onClick={onRemoveDefinition.bind(null, i)}>{'×'}</button>
      </li>
    );
  });

  const error_JSX = (error) ? (
    <p className="form-error">{error}</p>
  ) : null;

  return (
    <form ref={ref_form}>

      <div className='buttons'>
        <button className='btn-link' type='button' onClick={onSaveWord} disabled={!is_dirty}>{'save'}</button>
        <button className='btn-link red' type='button' onClick={onResetWord} disabled={!is_dirty}>{'reset'}</button>
        <button className='btn-link' type='button' onClick={props.onCloseWord}>{'close'}</button>
        {error_JSX}
      </div>

      <div className='field'>
        <label htmlFor='word-word'>{'word'}</label>
        <InputText id='word-word' value={form.word} onChange={val => setForm({word: N(val)})}/>
      </div>

      <div className='field'>
        <label htmlFor='word-type'>{'type'}</label>
        <InputRadioButtons options={['noun', 'verb', 'adjective', 'adposition', 'conjunction', 'affix', 'pronoun', 'name', 'other']} value={form.type} onChange={val => setForm({type: val})}/>
      </div>

      <div className='field'>
        <label htmlFor='word-pronunciation'>{'pronunciation'}</label>
        <InputText id='word-pronunciation' value={form.pronunciation} onChange={val => setForm({pronunciation: N(val)})}/>
      </div>

      <div className='field'>
        <label>{'parts'}</label>
        <ol ref={ref_parts}>{parts_JSX}</ol>
        <button className='btn-link' type='button' onClick={onAddPart}>{'+ add'}</button>
      </div>

      <div className='field'>
        <label>{'definitions'}</label>
        <ol ref={ref_definitions}>{definitions_JSX}</ol>
        <button className='btn-link' type='button' onClick={onAddDefinition}>{'+ add'}</button>
      </div>

    </form>
  );

}
