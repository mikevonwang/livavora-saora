import { N } from '../../global/js/utilities';

import InputText from '../../modules/inputtext/inputtext';

import { MainContext } from '../../pages/main/main';

export default function InputWord(props) {

  const [query, setQuery] = React.useState('');
  const [results, setResults] = React.useState([]);
  const [hilight_index, setHilightIndex] = React.useState(null);

  const all_words = React.useContext(MainContext).all_words;

  React.useEffect(() => {
    setResults(all_words.filter((word) => {
      return (word.word !== props.currentWord.word && word.word.indexOf(query) > -1);
    }));
  }, [query]);

  function onChangeQuery(value) {
    setQuery(N(value).toLowerCase());
  }

  function onBlur() {
    setQuery('');
    setResults([]);
  }

  function onMouseDown(e) {
    e.preventDefault();
  }

  function onClick(index) {
    props.onChange(results[index].id);
    setQuery('');
    setResults([]);
  }

  function onKeyDown(e) {
    switch (e.keyCode) {
      case 38:
        if (hilight_index === null) {
          if (hilight_index === null) {
            setHilightIndex(results.length - 1);
          }
          else if (hilight_index > 0) {
            setHilightIndex(hilight_index - 1);
          }
        }
      break;
      case 40:
        if (hilight_index === null) {
          setHilightIndex(0);
        }
        else if (hilight_index < results.length -1) {
          setHilightIndex(hilight_index + 1);
        }
      break;
      case 13:
        if (hilight_index !== null) {
          onClick(hilight_index);
        }
      break;
    }
  }

  let TSX = null;
  if (props.value === null) {
    let resultsTSX = null;
    if (query === '') {
      resultsTSX = null;
    }
    else if (results.length > 0) {
      resultsTSX = (
        <ul>
          {
            results.map((word, i) => {
              return (
                <li
                  key={i}
                  className={i === hilight_index ? 'hilight' : ''}
                  onMouseDown={onMouseDown}
                  onClick={onClick.bind(null, i)}
                >
                  {word.word}
                </li>
              );
            })
          }
        </ul>
      );
    }
    else {
      resultsTSX = (
        <p className='no-result'>{'No results found'}</p>
      );
    }

    TSX = (
      <>
        <InputText value={query} onChange={onChangeQuery} onKeyDown={onKeyDown} onBlur={onBlur}/>
        {resultsTSX}
      </>
    );
  }
  else {
    TSX = (
      <p className='word-value'>{all_words.find(word => word.id === props.value).word}</p>
    );
  }

  return (
    <div className='input-word' id={props.id}>
      {TSX}
    </div>
  );

}
