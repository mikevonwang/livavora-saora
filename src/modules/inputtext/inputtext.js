export default function InputText(props) {

  return (
    <input
      id = {props.id}
      className = {props.className}
      type = {'text'}
      value = {props.value}
      onChange = {(e) => props.onChange(e.target.value)}
      onKeyDown = {(props.onKeyDown) ? (e) => props.onKeyDown(e) : null}
      onBlur = {(props.onBlur) ? (e) => props.onBlur(e) : null}
      autoCorrect = 'off'
      autoCapitalize = 'off'
      autoComplete = 'username'
      spellCheck = 'false'
      disabled = {props.disabled}
    />
  );

}
