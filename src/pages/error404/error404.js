export default function Error404(props) {
  return (
    <main className='error-404'>
      <h1>{'Oops! It looks like we’re lost.'}</h1>
      <p>
        <span>{'Let’s go back to the '}</span>
        <A href={'/'}>{'main page'}</A>
        <span>{'.'}</span>
      </p>
      <p>{'Error: 404'}</p>
    </main>
  );
}
