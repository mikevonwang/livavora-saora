import XHR from '../../global/js/xhr';
import { N } from '../../global/js/utilities';

import Form from '../../modules/form/form';
import InputText from '../../modules/inputtext/inputtext';
import InputRadioButtons from '../../modules/inputradiobuttons/inputradiobuttons';
import Tooltip from '../../modules/tooltip/tooltip';

import useSearch from '../../hooks/useSearch';

export default function Main(props) {

  const [loaded, setLoaded] = React.useState(false);
  const [form_id, setFormId] = React.useState(null);
  const [confirm_remove_id, setConfirmRemoveId] = React.useState(null);
  const [search_mode, setSearchMode] = React.useState(null);
  const [all_words, filtered_words, search_query, setWords, setSearchQuery] = useSearch(null, '', search_mode);

  const confirming_remove_button = React.useRef(null);

  React.useEffect(() => {
    async function getWords() {
      const result = await XHR.call('get', '/words', {});
      setWords(result);
      setLoaded(true);
    }
    if (loaded === false) {
      getWords();
    }
  }, [loaded]);

  React.useEffect(() => {
    document.addEventListener('keydown', onKeyDown);
    return (() => {
      document.removeEventListener('keydown', onKeyDown);
    });
  }, [all_words]);

  // - - - - -

  function onClickWord(id) {
    if (id === form_id) {
      setFormId(null);
    }
    else {
      setFormId(id);
    }
  }

  function onKeyDown(e) {
    if (e.target === document.body && e.keyCode === 65) {
      onAddWord();
    }
  }

  async function onAddWord() {
    const id = new Date().getTime();
    const new_word = {
      id: id,
      word: '',
      type: 'noun',
      pronunciation: '',
      parts: null,
      definitions: [''],
    };
    const new_words = [new_word, ...all_words];
    setWords(new_words);
    setSearchQuery('');
    setTimeout(() => {
      setFormId(id);
    });
    await XHR.call('post', '/words', {
      word: new_word,
    });
  }

  function onClickRemoveButton(id, index) {
    confirming_remove_button.current = document.querySelector('li.entry:nth-child(' + (index + 1) +  ') button.remove');
    setConfirmRemoveId(id);
  }

  function onBlurRemoveButton() {
    confirming_remove_button.current = null;
    setConfirmRemoveId(null);
  }

  async function onRemoveWord(id) {
    const new_words = all_words.filter((word) => {
      return (word.id !== id);
    });
    setWords(new_words);
    setConfirmRemoveId(null);
    if (form_id === id) {
      setFormId(null);
    }
    await XHR.call('delete', '/words/' + id, {});
  }

  function onSaveWord(new_word) {
    return new Promise(async (resolve, reject) => {
      if (all_words.find((word) => {
        return (word.word === new_word.word && word.type === new_word.type && word.id !== form_id);
      }) !== undefined) {
        reject('word_already_exists');
      }
      else {
        const new_words = all_words.map((word) => {
          if (word.id === form_id) {
            return new_word;
          }
          else {
            return word;
          }
        }).sort((a, b) => {
          return a.word.localeCompare(b.word);
        });
        setWords(new_words);
        await XHR.call('put', '/words/' + new_word.id, {
          word: new_word,
        });
        resolve();
      }
    });
  }

  function onChangeSearchQuery(value) {
    setFormId(null);
    setSearchQuery(N(value));
  }

  function onCloseWord() {
    setFormId(null);
  }

  // - - - - -

  const word_JSX = filtered_words ? filtered_words.map((word, i) => {
    const word_word_JSX = (word.word) ? (
      <p className='word'>
        <strong>{word.word}</strong>
        {' /' + word.pronunciation + '/ '}
        <em>{word.type}</em>
      </p>
    ) : null;
    const definition_JSX = word.definitions.map((definition, j) => {
      return (
        <li key={j}><p>{definition}</p></li>
      );
    });
    const parts_JSX = word.parts ? (
      <p className='parts'>{'from '}{word.parts.map((part, k) => {
        return R((k > 0 ? ' + ' : '') + '*(' + all_words.find(word => word.id === part).word + ')');
      })}</p>
    ) : null;
    const class_current = (word.id === form_id) ? ' current' : '';
    const is_deletion_tooltip_visible = (word.id === confirm_remove_id);
    const text_edit = (word.id === form_id) ? 'close' : 'edit';
    return (
      <li className={'entry' + class_current} key={word.id}>
        {word_word_JSX}
        {parts_JSX}
        <ol className='definitions'>
          {definition_JSX}
        </ol>
        <div className='buttons'>
          <button className='btn-link edit' onClick={onClickWord.bind(null, word.id)}>{text_edit}</button>
          <button className='btn remove' onClick={onClickRemoveButton.bind(null, word.id, i)} onBlur={onBlurRemoveButton}>{'×'}</button>
        </div>
      </li>
    );
  }) : null;

  const form_JSX = (form_id !== null) ? (
    <Form
      word={filtered_words.find(word => word.id === form_id)}
      onSaveWord={onSaveWord}
      onCloseWord={onCloseWord}
    />
  ) : null;

  const clear_search_JSX = (search_query !== '') ? (
    <button className='btn remove' onClick={onChangeSearchQuery.bind(null, '')}>{'×'}</button>
  ) : null;

  const main = (loaded) ? (
    <main>
      <section className='sidebar'>
        <form className='search'>
          <div className='row'>
            <div className='field'>
              <label htmlFor='search-query'>{'search'}</label>
              <div className='search-field-container'>
                <InputText id='search-query' value={search_query} onChange={onChangeSearchQuery}/>
                {clear_search_JSX}
              </div>
            </div>
            <div className='field'>
              <label>{'languages'}</label>
              <InputRadioButtons options={['säora','english']} value={search_mode} onChange={setSearchMode} allow_null={true}/>
            </div>
          </div>
        </form>
        <button className='btn-link' onClick={onAddWord}>{'+ add'}</button>
        <ul className='words'>
          {word_JSX}
        </ul>
      </section>
      {form_JSX}
      <div className='tooltip-container'>
        <Tooltip visible={(confirm_remove_id !== null)} parent={confirming_remove_button.current}>
          <p>{'are you sure?'}</p>
          <button className='btn remove' onClick={onRemoveWord.bind(null, confirm_remove_id)} onMouseDown={e => e.preventDefault()}>{'×'}</button>
        </Tooltip>
      </div>
    </main>
  ) : null;

  return (
    <MainContext.Provider value={{all_words}}>
      <header>
        <h1>{'lïvavora säora'}</h1>
      </header>
      {main}
    </MainContext.Provider>
  );

}

export const MainContext = React.createContext(null);
