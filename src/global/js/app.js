import XHR from './xhr';
import Utilities from './utilities';

import Main from '../../pages/main/main';

import Error404 from '../../pages/error404/error404';

class App extends React.Component {

  constructor(props) {
    super(props);

    let env = null;
    switch (location.hostname) {
      case Config.env.local.location_hostname:
        env = 'local';
      break;
      case Config.env.live.location_hostname:
        env = 'live';
        console.log = () => {};
      break;
    }

    this.state = {
      env: env,
      masked: false,
    };
  }

  setRoot(new_state_items) {
    this.setState(new_state_items);
  }

  render() {
    return (
      <>
        {Rhaetia.renderChild(this.props.children, {
          root: this.state,
          setRoot: this.setRoot.bind(this),
        })}
        <div className={'mask ' + (this.state.masked ? 'active' : '')}/>
      </>
    );
  }

};

const route_tree = [
  ['', Main],
];

ReactDOM.render((
  <Rhaetia.Router routeTree={route_tree} page404={Error404}>
    <App/>
  </Rhaetia.Router>
), document.getElementById('root'));
