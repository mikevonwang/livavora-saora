import Utilities from './utilities';

export const Archive = Utilities.filter_unpublished_posts(Utilities.add_date_to_posts([

  // 000 - - - - -
  {
    slug: 'the-search-for-natural-css-transforms',
    date: '2019-03-01',
    localized: {
      en: {
        title: 'The search for natural CSS transforms',
      },
      de: {
        title: 'Die Suche nach natürliche CSS-Transforms',
      },
      ra: {
        title: 'zuzuhen CSS-xirkínra lävaï nïr',
      },
    },
    published: true,
  },

  // 001 - - - - -
  {
    slug: 'understanding-manual-camera-exposure',
    date: '2019-03-31',
    localized: {
      en: {
        title: 'Understanding manual camera exposure',
      },
      de: {
        title: 'Ein Verständnis von manuelle Kamerabelichtung',
      },
      ra: {
        title: 'ïaoren keu zuinlïana kaera manï',
      },
    },
    published: true,
  },

]));
