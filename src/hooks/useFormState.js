export default function useFormState(initial_form) {
  const [form, setForm] = React.useState(initial_form);
  const [is_dirty, setIsDirty] = React.useState(false);

  function onSetForm(new_form_parts) {
    setIsDirty(true);
    setForm(Object.assign({}, form, new_form_parts));
  }

  function resetForm() {
    setIsDirty(false);
    setForm(initial_form);
  }

  return [form, onSetForm, is_dirty, setIsDirty, resetForm];
}
