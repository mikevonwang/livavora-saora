export default function useSearch(list_initial, query_initial, search_mode) {
  const [all_items, setAllItems] = React.useState(list_initial);
  const [filtered_items, setFilteredItems] = React.useState(list_initial);
  const [search_query, setSearchQuery] = React.useState(query_initial);

  React.useEffect(() => {
    setFilteredItems(search(all_items, search_query, search_mode));
  }, [search_query, search_mode, all_items]);

  return [all_items, filtered_items, search_query, setAllItems, setSearchQuery];
}

function search(word_list, query_raw, search_mode) {

  function match_säora(word, query) {
    return (word.word.toLowerCase().indexOf(query) > -1);
  }

  function match_english(word, query) {
    return (word.definitions.find((definition) => {
      return (definition.toLowerCase().indexOf(query) > -1);
    }) !== undefined);
  }

  if (word_list) {
    if (query_raw !== '') {
      const query = query_raw.toLowerCase();
      switch (search_mode) {
        case 'säora':
          return word_list.filter((word) => {
            return (match_säora(word, query));
          });
        case 'english':
          return word_list.filter((word) => {
            return (match_english(word, query));
          });
        default:
          return word_list.filter((word) => {
            return (match_säora(word, query) || match_english(word, query));
          });
      }
    }
    else {
      return word_list
    }
  }
  else {
    return null;
  }
}
