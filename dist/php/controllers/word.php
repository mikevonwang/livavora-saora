<?php

  namespace Mieda;

  class Word extends Controller {

    public static function get_all() {
      if (self::c()) {
        $data = json_decode(file_get_contents(__DIR__ . '/../../db/words.json', FILE_USE_INCLUDE_PATH));
        self::$result = $data;
      }
      return self::output();
    }

    public static function add() {
      if (self::c()) {
        $data = json_decode(file_get_contents(__DIR__ . '/../../db/words.json', FILE_USE_INCLUDE_PATH));
        $data[] = self::$data['word'];
        file_put_contents(__DIR__ . '/../../db/words.json', json_encode($data), FILE_USE_INCLUDE_PATH);
        self::$result = 'word_added_successfully';
      }
      return self::output();
    }

    public static function delete() {
      if (self::c()) {
        $data = json_decode(file_get_contents(__DIR__ . '/../../db/words.json', FILE_USE_INCLUDE_PATH));
        $data = array_values(array_filter($data, function($word) {
          if ($word->id == self::$data['id']) {
            return false;
          }
          else {
            return true;
          }
        }));
        file_put_contents(__DIR__ . '/../../db/words.json', json_encode($data), FILE_USE_INCLUDE_PATH);
        self::$result = 'word_deleted_successfully';
      }
      return self::output();
    }

    public static function update() {
      if (self::c()) {
        $data = json_decode(file_get_contents(__DIR__ . '/../../db/words.json', FILE_USE_INCLUDE_PATH));
        $data = array_values(array_map(function($word) {
          if ($word->id == self::$data['id']) {
            return self::$data['word'];
          }
          else {
            return $word;
          }
        }, $data));
        file_put_contents(__DIR__ . '/../../db/words.json', json_encode($data), FILE_USE_INCLUDE_PATH);
        self::$result = 'word_updated_successfully';
      }
      return self::output();
    }

  }

?>
