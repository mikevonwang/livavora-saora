<?php

  namespace Mieda;

  $router = new Router();

  $router->get(    'words', 'Word::get_all');
  $router->post(   'words', 'Word::add');

  $router->delete( 'words/:id', 'Word::delete');
  $router->put(    'words/:id', 'Word::update');

?>
